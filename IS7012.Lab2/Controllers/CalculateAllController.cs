﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS7012.Lab2.Controllers
{
    public class CalculateAllController : Controller
    {

        public string Add([Bind(Prefix = "number")]int[] numbers)
        {

            /* I feel like this a bit of an abuse of the bind attribute (my understanding is that 
             * is is not meant for this use but it seems to work so we'll run with it).
             * Using the Bind attribute allows us to specify that the parameter in the
             * query string will be called 'number' but binds this value to
             * our 'numbers' parameter
             */

            if (numbers != null)
            {
                int aggregate = 0;

                foreach (int number in numbers)
                {
                    aggregate = aggregate + number;
                }

                return string.Format("{0} = {1}", string.Join(" + ", numbers), aggregate);
            }

            return "Please provide a list of numbers";
        }




        public string Subtract([Bind(Prefix = "number")]int[] numbers)
        {
            if (numbers != null)
            {
                int aggregate = numbers[0];

                for (int i = 1; i < numbers.Length; i++) // note the starting point is 1 not 0
                {
                    var number = numbers[i];
                    aggregate = aggregate - number;
                }

                return string.Format("{0} = {1}", string.Join(" - ", numbers), aggregate);
            }

            return "Please provide a list of numbers";
        }




        public string Multiply([Bind(Prefix = "number")]int[] numbers)
        {
            if (numbers != null)
            {
                int aggregate = numbers[0];

                foreach (int number in numbers.Skip(1)) // Skip is a LINQ method that allows us to iterate a list and skip over
                {                                       // the specified number of items in the array (in this case 1)
                    aggregate = aggregate * number;
                }

                return string.Format("{0} = {1}", string.Join(" x ", numbers), aggregate);
            }

            return "Please provide a list of numbers";
        }
    }
}
