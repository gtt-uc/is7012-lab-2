﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS7012.Lab2.Controllers
{
    public class ArtistsController : Controller
    {

        // GET method
        public string Index()
        {
            /*
             * 1. connect to database
             * 2. Query for all artists - ordered by name
             * 3. if artists found then return view listing artists
             * 4. if no artists found the display error message
             */

            return "all artists";
        }

        // GET method
        public string Search(string q, int? page)
        {
            /*
             * 1. connect to database
             * 2. Query for artists matching query
             *    Get page of records matching the page number (assume 25 records per page)
             *    Order records by name
             * 3. if artists found then return view listing artists with navigation controls
             * 4. if no artists found the display error message
             */
            return "search artists";
        }

        // GET method
        public string Display(int id)
        {
            /*
             * 1. connect to database
             * 2. Search for artist with an id matching the id parameter
             * 3. if artist is found then return view with artist details
             * 4. if artist is not found display an error message (or return a 404 error)
             * */

            return "an artists instance";
        }

        // GET method
        public string Create()
        {
            /*
             * 1. return a view with the form to create an artist
             */
            return "Create Artist GET";
        }

        // POST method
        public string Create(string artistName, bool soloArtist)
        {
            /*
             * 1. validate input data
             * 2. if input data is not valid return view displaying form with data pre-filled
             *    and indicate the errors
             * 3. if input data is value the query database for exact match
             * 4. if artist found that is exact match then return view displaying form with data pre-filled
             *    and indicate the problem (duplicate record)
             * 5. if artist is not duplicate then create the artist record in the database
             * 6. redirect the user to the index page and display a success message
             */
            return "Create Artist POST";
        }

        // GET Method
        public string Edit(int? id)
        {
            /*
             * 1. connect to database
             * 2. Search for artist with an id matching the id parameter
             * 3. if artist is found then return edit form with artist details pre-filled
             * 4. if artist is not found display an error message (or return a 404 error)
             * */
            return "edit artist";
        }

        // POST method
        public string Edit(int id, string artistName, bool soloArtist)
        {
            /* 1. Connect to database
             * 2. Check to ensure artist with id equal to id paramter exists
             * 3. if artist with id does not exist then return error (or 404)
             * 4. validate input data
             * 5. if input data is not valid return view displaying form with data pre-filled
             *    and indicate the errors
             * 6. Update the existing data with the new data
             * 6. redirect the user to the index page and display a success message
             */
            return "Create Artist POST";
        }

        // POST method
        public string Delete(int id)
        {
            /* 1. Connect to database
             * 2. Check to ensure artist with id equal to id paramter exists
             * 3. if artist with id does not exist then return error (or 404)
             * 4. Delete the artist with the id matching the id parameter
             * 6. redirect the user to the index page and display a success message
             */
            return "Create Artist POST";
        }

    }
}
