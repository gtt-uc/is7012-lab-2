﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS7012.Lab2.Controllers
{
    public class CalculateBetterController : Controller
    {

        public string Add(int? number1, int? number2)
        {
            if (number1.HasValue && number2.HasValue)
            {
                int result = number1.Value + number2.Value;
                return String.Format("{0} + {1} = {2}", number1, number2, result);
            }

            return "Plese provide 2 numbers";
        }

        public string Subtract(int? number1, int? number2)
        {
            if (number1.HasValue && number2.HasValue)
            {
                float result = number1.Value - number2.Value;
                return String.Format("{0} - {1} = {2}", number1, number2, result);
            }

            return "Plese provide 2 numbers";
        }

        public string Multiply(int? number1, int? number2)
        {
            if (number1.HasValue && number2.HasValue)
            {
                int result = number1.Value * number2.Value;
                return String.Format("{0} x {1} = {2}", number1, number2, result);
            }

            return "Plese provide 2 numbers";
        }

        public string Divide(int? number1, int? number2)
        {
            if (number1.HasValue && number2.HasValue)
            {
                if (number2.Value == 0)
                {
                    return "Cannot divide by zero";
                }
                else
                {
                    int result = number1.Value / number2.Value;
                    return String.Format("{0} / {1} = {2}", number1, number2, result);
                }
            }

            return "Plese provide 2 numbers";
        }

    }
}
