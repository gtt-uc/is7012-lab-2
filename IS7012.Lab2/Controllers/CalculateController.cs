﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS7012.Lab2.Controllers
{
    public class CalculateController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public string Add(int number1, int number2)
        {
            int result = number1 + number2;
            return String.Format("{0} + {1} = {2}", number1, number2, result);
        }

        public string Subtract(int number1, int number2)
        {
            int result = number1 - number2;
            return String.Format("{0} - {1} = {2}", number1, number2, result);
        }

        public string Multiply(int number1, int number2)
        {
            int result = number1 * number2;
            return String.Format("{0} x {1} = {2}", number1, number2, result);
        }

        public string Divide(int number1, int number2)
        {

            if (number2 == 0)
            {
                return "Cannot divide by zero";
            }
            else
            {
                int result = number1 / number2;
                return String.Format("{0} / {1} = {2}", number1, number2, result);
            }
        }

    }
}
