﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS7012.Lab2.Controllers
{
    public class UsStatesLookupController : Controller
    {
        System.Collections.Specialized.NameValueCollection _usStates = new System.Collections.Specialized.NameValueCollection();

        public UsStatesLookupController()
        {
            _usStates["OH"] = "Ohio";
            _usStates["KY"] = "Kentucky";
            _usStates["FL"] = "Florida";
        }

        // GET: /UsStatesLookup/display/OH
        public string Display(string id)
        {

            if (string.IsNullOrEmpty(id))
            {
                return "Please enter a state abbreviation";
            }

            var usState = _usStates[id]; // usState will either be null or have the value from the collection

            if (usState == null)
            {
                return String.Format("No state found with the abbreviation = {0}", id);
            }

            return string.Format("State matching {0} is {1}", id, usState);
        }

    }
}
